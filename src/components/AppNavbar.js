import { useContext } from 'react';
import { Container, Navbar, Nav, Image} from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

import Logo from '../images/logo3.png'

export default function AppNavbar() {
  const { user } = useContext(UserContext);


  return (
    <Navbar expand="lg">
      <Container fluid>
        <Navbar.Brand as={Link} to="/">
          <Image src={Logo} alt="Logo" width={300} height={100} />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">
            {user.id !== null && !user.isAdmin && (
              <Nav.Link as={NavLink} to="/">
                Home
              </Nav.Link>
            )}

            {user.id !== null && user.isAdmin && (
              <Nav.Link as={NavLink} to="/addProduct">
                Add Product
              </Nav.Link>
            )}

            {user.id !== null && user.isAdmin && (
              <Nav.Link as={NavLink} to="/allProducts">
                All Products
              </Nav.Link>
            )}

            {!user.isAdmin && (
              <Nav.Link as={NavLink} to="/products">
                Products
              </Nav.Link>
            )}

            {/*{user.id !== null && !user.isAdmin && (
              <Nav.Link as={NavLink} to="/details">
                MyCheckout
              </Nav.Link>
            )}*/}

            {user.id !== null ? (
                <Nav.Link as={NavLink} to="/logout">
                  Logout
              </Nav.Link>
            ) : (
              <>
                <Nav.Link as={NavLink} to="/login">
                  Login
                </Nav.Link>
                <Nav.Link as={ NavLink } to="/register">Register</Nav.Link>
            </>
            )
          }     
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    )
}

/*import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({ product }) {

    const { _id, name, description, price } = product;


    return (
        <Card className="my-3">
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{ price }</Card.Text>
                <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}
*/


import { Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';


import productImg2 from "../images/jordan2.jpg";




export default function ProductCard({ product }) {


    
    
    const productImages = [
    productImg2,
  ];

    const randomIndex = Math.floor(Math.random() * productImages.length);
  const randomProductImage = productImages[randomIndex];

    const { _id, name, description, price } = product;


    return (
        <Card className="my-3 product-card">
            <Card.Img src={randomProductImage} />
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{ description }</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>${ price }</Card.Text>
                <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
            </Card.Body>
        </Card>
    )
}

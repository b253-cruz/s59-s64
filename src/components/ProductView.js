import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
  const { productId } = useParams();

  const { user } = useContext(UserContext);
  const navigate = useNavigate();

  const [ productName, setProductName ] = useState('');
  const [ userId, setUserId] = useState(user.id);
  const [ name, setName ] = useState("");
  const [ description, setDescription ] = useState("");
  const [ price , setPrice ] = useState(0);
  const [quantity, setQuantity] = useState(0);
  const [ totalAmount, setTotalAmount ] = useState(0);

useEffect(() => {

    console.log(productId);

    fetch(`http://localhost:4000/products/${productId}`)
    .then(res => res.json())
    .then(data => {

      console.log(data);

      setName(data.name);
      setDescription(data.description);
      setPrice(data.price);
      setQuantity(data.quantity);
      setProductName(data.name)
    })

  }, [ productId ]);

useEffect(() => {
  setTotalAmount(price*quantity);
}, [price, quantity])


   const enroll = (productId) => {
      if (quantity < 1) {
          Swal.fire({
              title: "Invalid quantity",
              icon: "error",
              text: "Please enter a quantity of at least 1."
          });
          return;
      }

      fetch(`http://localhost:4000/users/checkout`,
      {
          method: 'POST',
          headers: {
              "Content-Type": "application/json",
              "Authorization": `Bearer ${localStorage.getItem('token')}`
          },
          body: JSON.stringify({
              productId: productId,
              productName: productName,
              quantity: quantity,
              price: price,
              userId: user.id  
          })
      })
      .then(res => res.json())
      .then(data => {

          console.log(data);

          if (data){

              Swal.fire({
                  title: "Successfully Checkout",
                  icon: 'success',
                  text: "You have successfully checkout this product."
              });
              navigate("/products");

          } else {

              Swal.fire({
                  title: "Something went wrong",
                  icon: "error",
                  text: "Please try again."
              });
          }
      }).catch(error => {
        Swal.fire({
          title: "Error",
          icon: "error",
          text: error.message
        })
      })
  }

  return (

    <Container className="mt-5">
      <Row>
        <Col lg={{ span: 5, offset: 3}}>
          <Card className="my-3">
              <Card.Body>
                  <Card.Title>{ name }</Card.Title>
                  <Card.Subtitle>Description:</Card.Subtitle>
                  <Card.Text>{ description }</Card.Text>
                  <Card.Subtitle>Price:</Card.Subtitle>
                  <Card.Text>Php { price }</Card.Text>

                  <Card.Subtitle>Quantity:</Card.Subtitle>
                  <input type="number" value={quantity} onChange={(e) => {
                      const newQuantity = Math.max(parseInt(e.target.value), 0);
                      setQuantity(newQuantity);
                  }} /><br></br>
                  <Card.Subtitle>Total Amount: Php {totalAmount}</Card.Subtitle>
                  { user.id !== null ? 
                      <Button variant="primary" block onClick={() => enroll(productId)}>Checkout</Button>
                    : 
                      <Link className="btn btn-danger btn-block" to="/login">Log in to Checkout</Link>
                  }
              </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )

}

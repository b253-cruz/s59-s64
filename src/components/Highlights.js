// Bootstrap grid system components
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
	return (
		<Row className="mt-3 mb-3">
			<Col xs={12} md={12}>
				<Card className="cardHighlight p-3">
			      <Card.Body>
			        <Card.Title>
			        	<h2>Welcome to Jordan Kicks!</h2>
			        </Card.Title>
			        <Card.Text>
			          Welcome to Jordan Kicks, the ultimate destination for sneaker enthusiasts and sports fans alike. At Jordan Kicks, we offer a wide range of high-quality athletic footwear, apparel, and accessories inspired by the iconic Michael Jordan. Our collection features the latest releases and classic designs that have become synonymous with the Jordan brand, all crafted with precision and attention to detail. Whether you're looking for sneakers to elevate your game on the court or to add a touch of style to your everyday look, we've got you covered. Our knowledgeable and passionate staff are dedicated to providing exceptional customer service and helping you find the perfect pair of sneakers to suit your needs. Come visit us and experience the Jordan Store difference today.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}

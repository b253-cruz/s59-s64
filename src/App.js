import { useState, useEffect } from 'react';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Routes } from 'react-router-dom';

import './App.css';

import AppNavbar from './components/AppNavbar';
import Products from './pages/Products';
import ProductView from './components/ProductView';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import Dashboard from './components/Dashboard';
import AdminProducts from './pages/AdminProducts';
import AdminView from './components/AdminView';
import AddProduct from './components/AddProduct';
import MyCheckout from './components/MyCheckout'


import { UserProvider } from './UserContext'

function App() {

  const storedUser = JSON.parse(localStorage.getItem('user'));
  const [ user, setUser ] = useState(storedUser || {
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() =>{
    localStorage.setItem('user', JSON.stringify(user));
  }, [user])

  return (

    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>      
        <Container fluid>   
          <AppNavbar />     
            <Routes>
              <Route path="/" element={<Home />} />
              <Route path="/products" element={<Products />} />
              <Route path="/product/:productId" element={<AdminView />} />
              <Route path="/products/:productId" element={<ProductView />} />
              <Route path="/login" element={<Login />} />
              <Route path="/logout" element={<Logout />}/>
              <Route path="/register" element={<Register />} />
              <Route path="/allProducts" element={<AdminProducts />} />
              <Route path="/addProduct" element={<AddProduct />} />
              <Route path="/details" element={<MyCheckout />} />
              <Route path="*" element={<Error />} />
            </Routes>
        </Container>
      </Router>
    </UserProvider>
    
  );
}

export default App;

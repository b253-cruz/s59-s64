const productData = [
    {
        id: "64097417340119cb5e28aed4",
        name: "Air Jordan Retro 1 Low",
        description: "Inspired by original AJ1",
        price: 6595.00,
        onOffer: true
    },
    {
        id: "640974cd340119cb5e28aed8",
        name: "Air Jordan 3 Retro",
        description: "Michael Jordan's one of the most sought after sneakers to-date ",
        price: 18595.00,
        onOffer: true
    },
    {
        id: "6409778f28981ca43d760d06",
        name: "Air Jordan 4 Retro",
        description: "Second Air Jordan designed by Tinker Hattfield ",
        price: 27461.00,
        onOffer: true
    }
    
]

export default productData;
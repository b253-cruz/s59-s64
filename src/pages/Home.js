/*import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {

	const data = {
	    title: "Jordan Kicks",
	    content: "Step up your game with Jordan kicks from the ultimate sneaker store.",
	    destination: "/products",
	    label: "Buy now!"
	}

	return (
		<>
			<div className="banner">
				<Banner data={ data } />
			</div>
			<div className="highlights">
				<Highlights />
			</div>
		</>
	)
}

*/

import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import Footer from '../components/Footer';

export default function Home() {
  const data = {
    title: 'Jordan Kicks',
    content:
      'Step up your game with Jordan kicks from the ultimate sneaker store.',
    destination: '/products',
    label: 'Buy now!',
  };

  return (
    <>
      <div className="banner">
        <Banner data={data} />
      </div>
      <div className="highlights">
        <Highlights />
      </div>
      <Footer />
    </>
  );
}
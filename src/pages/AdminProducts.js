import { useEffect, useState } from "react";
import Dashboard from "../components/Dashboard";

export default function Products() {
  const [products, setProducts] = useState([]);

  useEffect(() => {
    fetch(`http://localhost:4000/products/allProducts`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setProducts(
          data.map((product) => {
            return <Dashboard key={product._id} product={product} />;
          })
        );
      });
  }, []);

  return (
      <div className="products-container">
      { products }
    </div>
    )
}
